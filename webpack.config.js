const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const templateParameters = {
  development: {
    MOMOS_API_URL: 'http://localhost:3060'
  }
};

module.exports = (env, args) => ({
  mode: args.mode,
  entry: './src/index.js',
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'js/main.[contenthash].js',
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: args.mode !== 'development',
      protectWebpackAssets: args.mode !== 'development',
    }),
    new HtmlWebpackPlugin({
      template: 'public/index.html',
      templateParameters: templateParameters[args.env],
    }),
    new CopyPlugin({
      patterns: [
        'public/404.html',
        'public/favicon.ico',
        'public/manifest.json',
        'public/robots.txt',
      ],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.join(__dirname, '/src'),
        use: ['babel-loader'],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  devtool: args.mode === 'development' ? 'source-map' : false,
  devServer: {
    contentBase: './dist',
    compress: true,
    historyApiFallback: true,
  },
});
