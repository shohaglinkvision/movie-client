import React, { useState, useContext } from "react";
import LoadingOverlay from 'react-loading-overlay';
import { Spinner } from "react-bootstrap";

const OverlayContext = React.createContext();
const OverlayUpdateContext = React.createContext();

export const useOverlay = () => useContext(OverlayContext);
export const useOverlayUpdate = () => useContext(OverlayUpdateContext);

export function OverlayProvider({ children }) {
  const [overlay, setOverlay] = useState({ isActive: false, text: "" });

  return (
    <OverlayContext.Provider value={overlay}>
      <OverlayUpdateContext.Provider value={setOverlay}>
        <OverlayContext.Consumer>
          {({ isActive, text }) => (
            <LoadingOverlay
              active={isActive}
              text={text}
              spinner={<Spinner animation="border" size="lg" />}
            >
              {children}
            </LoadingOverlay>
          )}
        </OverlayContext.Consumer>
      </OverlayUpdateContext.Provider>
    </OverlayContext.Provider>
  );
}

export default OverlayContext;
