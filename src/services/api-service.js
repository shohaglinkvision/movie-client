import axios from 'axios';

let client = axios.create({
  baseURL: window.RESOURCES.MOMOS_API_URL,
  headers: {
    'Content-Type': 'application/json'
  },
});

const get = async (url, params) => {
  const resp = await client.get(url, { params });
  return resp.data;
};

const getMovies = () =>
get(`/popular-movies`);

export default client;
export {
  getMovies
};
