import React from 'react';
import { Route, Switch } from 'react-router-dom';
import MovieDetailsView from './components/MovieDetailsView';
import MovieList from './components/MovieList';
import { OverlayProvider } from './states/overlay-context';

function App(props) {
  return (
    <OverlayProvider>
        <Switch>
          <Route exact path="/" component={MovieList} />
          <Route exact path="/movie/:id" component={MovieDetailsView} />
        </Switch>
    </OverlayProvider>
  );
}

export default App;
