import React from 'react'
import { Link } from 'react-router-dom'

export default function Movie(props) {
  const getPoster = () => {
    const poster = 'https://image.tmdb.org/t/p/w342' + props.movie.poster_path
    return poster
  }

  const getRating = () => {
    const rating = props.movie.vote_average
    return rating
  }
  return (
    <div className='col-sm-3'>
        <div className='card bg-dark text-white border-0'>
          <div>
            <div className='img-zoom-container'>
              <div className='img-zoom'>
              <Link to={{
                    pathname: `/movie/${props.movie.id}`,
                    state: {movie: props.movie}
                }}>
                <img className='card-img-top' alt='movie poster' src={getPoster()}/> 
               </Link>
              </div>
            </div>
            <div className='badge-circle badge-warning position-absolute mt-4 right-badge'>★{getRating()}/10</div>
          </div>
          <div className='card-body'>
            <h2>{props.movie.title}</h2>
          </div>
        </div>
    </div>
  )
}
