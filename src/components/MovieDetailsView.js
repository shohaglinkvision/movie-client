import React from 'react';
import { Container } from 'react-bootstrap'
import { useLocation } from "react-router-dom";

export default function MovieDetailsView(props) {
  const { state } = useLocation();
  const getPoster = () => {
    const poster = 'https://image.tmdb.org/t/p/w342' + state.movie.poster_path
    return poster
  }

  const getRating = () => {
    const rating = state.movie.vote_average
    return rating
  }

  return (
    <Container fluid>
      <div className='row'>
        <div className='col-sm-3'>
          <div className='card bg-dark text-white border-0'>
            <div>
              <div>
                <img className='card-img-top' alt='movie poster' src={getPoster()}/> 
              </div>
            </div>
            <div className='card-body'>
            </div>
          </div>
        </div>
        <div className='col-sm-9'>
          <h2>{state.movie.title}</h2>
          <p className='badge-circle badge-warning position-absolute mt-4 right-badge'>★{getRating()}/10</p>
          <h2>Overview</h2>
          <p>{state.movie.overview}</p>
        </div>
      </div>
    </Container>
  )
}
