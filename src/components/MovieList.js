import React, { useEffect, useState } from 'react';
import { Container, Tabs, Tab } from 'react-bootstrap';
import { getMovies } from '../services/api-service';
import Movie from './Movie';

export default function MovieList(props) {
    const [movies, setMovies] = useState([]);

    useEffect(() => {
        const movieList = getMovies();
        movieList.then((movies)=>{
            setMovies(movies.items);
        }).catch((err)=> {})
    }, [props.movies]);
    
    return (
        <Container fluid>
            <div style={{
                position: "absolute",
                left: '10%',
                marginLeft: "-100px",
            }}>
                <Tabs defaultActiveKey="latest" id="uncontrolled-tab-example">
                    <Tab eventKey="latest" title="Latest movie">
                    <div className='bg-dark py-2'>
                        <div className='container'>
                            <div className='row'>
                                {movies.map(movie => (
                                <Movie key={movie.id} movie={movie} />
                                ))}
                            </div>
                        </div>
                    </div>
                    </Tab>
                    <Tab eventKey="popular-movie" title="Popurlar movie">
                    <h1>movie popular feature is under construction</h1>
                    </Tab>
                </Tabs>
            </div>
        </Container>
    );
}